#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

# https://wiki.debian.org/HardeningWalkthrough
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DH_GOPKG := github.com/google/fscrypt

### Set some build flags.
### Partially borrowed from upstream Makefile

GO_LINK_FLAGS := -s -w
VERSION_FLAG := -X "main.version=v$(DEB_VERSION_UPSTREAM)"
GO_LINK_FLAGS += $(VERSION_FLAG) -extldflags "$(LDFLAGS)"
GO_FLAGS += --ldflags '$(GO_LINK_FLAGS)'

%:
	dh $@ --buildsystem=golang --with=golang

override_dh_auto_build:
	# Build the command line tool
	DH_GOLANG_BUILDPKG=$(DH_GOPKG)/cmd/... dh_auto_build -O--buildsystem=golang -- $(GO_FLAGS)

	# Build the PAM module (C shared library)
	DH_GOLANG_BUILDPKG=$(DH_GOPKG)/pam_fscrypt/... dh_auto_build -O--buildsystem=golang -- $(GO_FLAGS) -buildmode=c-shared

	# Workaround for https://github.com/golang/go/issues/24253
	# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=903385
	#
	# golang-go calls the library pam_fscrypt.a, while gccgo-go calls it libpam_fscrypt.a.
	# One thing we (hopefully) know: there is only one .a file. Its path is also difficult
	# to predict. Let's try to write a command that will always rename it to pam_fscrypt.so.
	ls -l obj-*/pkg/*/$(DH_GOPKG)/*.a
	SOPATH=$$(dirname obj-*/pkg/*/$(DH_GOPKG)/*.a) && mv -v $$SOPATH/*.a $$SOPATH/pam_fscrypt.so

override_dh_auto_test:
	# Excluded tests:
	#
	# keyring:     needs a user keyring linked into the session keyring
	#             https://github.com/google/fscrypt/issues/99
	# filesystem: requires a filesystem to test upon and root privileges

	DH_GOLANG_EXCLUDES="keyring filesystem" dh_auto_test

override_dh_auto_install:
	# See dh-golang(3)
	dh_auto_install -- --no-source

override_dh_install:
	dh_install

	# Adapted from the upstream Makefile
	m4 --define=PAM_INSTALL_PATH=pam_fscrypt.so \
		< pam_fscrypt/config \
		> $(CURDIR)/debian/libpam-fscrypt/usr/share/pam-configs/fscrypt
